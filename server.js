const express = require('express');
const app = express();
const PORT = process.env.PORT || 3000;

app.use(logger);

app.use(express.json());

const users = [];

app.get('/users', (req, res) => {
  res.json(users);
});

app.post('/users', (req, res) => {
  req.body.forEach((userId) => {
    const user = { name: userId.name, password: userId.password };
    users.push(user);
    res.status(201).send();
  });
});

app.post('/users/login', auth, (req, res) => {
  console.log(`hello ${req.body.name}`);
});

function auth(req, res, next) {
  const user = users.find((user) => user.name === req.body.name);
  if (user === undefined) {
    res.status(400).send('User not found');
  } else {
    if (user.password === req.body.password) {
      res.send('login succesful');
      next();
    } else {
      res.send('Wrong Password');
    }
  }
}

function logger(req, res, next) {
  console.log(`${req.protocol}://${req.get('host')}${req.originalUrl}`);
  next();
}

app.listen(PORT, () => console.log(`Server has started at ${PORT}`));
